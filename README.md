# Matter Properties and Transformations


This is an open lecture about properties and transformations of matter.

From its very beginning it is designed to be very interdisciplinary.

Therefore a common vocabulary will be developed in parallel in the formal Ontology Definition Language OWL.

This ontology is called the "open science ontology".

The mathematical background and tools  are also provided in separate repositories.